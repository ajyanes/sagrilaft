package com.data.sagrilaft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SagrilaftApplication {

  public static void main(String[] args) {
    SpringApplication.run(SagrilaftApplication.class, args);
  }

}
