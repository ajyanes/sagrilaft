package com.data.sagrilaft.mappers.entities;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ENTITY_ADDRESS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address implements Serializable {

  @XmlElement(name = "CITY")
  private String city="";

  @XmlElement(name = "COUNTRY")
  private String country="";

  @Override
  public String toString() {
    return "Address{" +
        "city='" + city + '\'' +
        ", country='" + country + '\'' +
        '}';
  }
}
