package com.data.sagrilaft.mappers.entities;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "ENTITY")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntityPrev implements Serializable {

  @XmlElement(name = "DATAID")
  private String dataId;
  @XmlElement(name = "VERSIONNUM")
  private Integer versionnum;
  @XmlElement(name = "FIRST_NAME")
  private String firstname;
  @XmlElement(name = "UN_LIST_TYPE")
  private String unlisttype;
  @XmlElement(name = "REFERENCE_NUMBER")
  private String referencenumber;
  @XmlElement(name = "LISTED_ON")
  private Date listedon;
  @XmlElement(name = "COMMENTS1")
  private String comments;
  @XmlElementRef(name = "LIST_TYPE",type = ListTypeEnt.class)
  private ListTypeEnt listtype;
  @XmlElement(name = "LAST_DAY_UPDATED")
  private String lastdateupdate;
  @XmlElement(name = "ENTITY_ALIAS")
  private String alias;
  @XmlElementRef(name = "ENTITY_ADDRESS",type = Address.class)
  private Address address;

}
