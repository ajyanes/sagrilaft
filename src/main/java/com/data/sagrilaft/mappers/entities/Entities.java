package com.data.sagrilaft.mappers.entities;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ENTITIES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Entities {

  @XmlElement(name = "ENTITY")
  private List<EntityPrev> entityPrevs = null;

  public List<EntityPrev> getEntitys(){return entityPrevs;}

}
