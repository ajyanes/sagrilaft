package com.data.sagrilaft.mappers.individuals;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "NATIONALITY")
@XmlAccessorType(XmlAccessType.FIELD)
public class Nationality implements Serializable {

  @XmlElement(name = "VALUE")
  private String value="";

  @Override
  public String toString() {
    return value;
  }
}
