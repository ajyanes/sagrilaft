package com.data.sagrilaft.mappers.individuals;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "Individual")
@XmlAccessorType(XmlAccessType.FIELD)
public class Individual implements Serializable {

  @XmlElement(name = "DATAID")
  private String dataId;
  @XmlElement(name = "VERSIONNUM")
  private Integer versionNum;
  @XmlElement(name = "FIRST_NAME")
  private String firstName;
  @XmlElement(name = "SECOND_NAME")
  private String secondName;
  @XmlElement(name = "THIRD_NAME")
  private String thirdName;
  @XmlElement(name = "UN_LIST_TYPE")
  private String unListType;
  @XmlElement(name = "REFERENCE_NUMBER")
  private String referenceNumber;
  @XmlElement(name = "LISTED_ON")
  private Date listedOn;
  @XmlElement(name = "COMMENTS1")
  private String comments;
  @XmlElementRef(name = "DESIGNATION",type = Designation.class)
  private Designation designation;
  @XmlElementRef(name = "NATIONALITY",type = Nationality.class)
  private Nationality nationality;
  @XmlElementRef(name = "LIST_TYPE",type = ListType.class)
  private ListType listType;
  @XmlElement(name = "LAST_DAY_UPDATED")
  private String lastDateUpdate;
  @XmlElement(name = "INDIVIDUAL_ALIAS")
  private String Alias;
  @XmlElement(name = "INDIVIDUAL_ADDRESS")
  private String indAddress;
  @XmlElementRef(name = "TYPE_OF_DATE",type = Birth.class)
  private Birth dateBirth;
  @XmlElementRef(name = "INDIVIDUAL_DOCUMENT",type = Document.class)
  private Document documentType;


}
