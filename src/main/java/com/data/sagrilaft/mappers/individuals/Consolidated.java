package com.data.sagrilaft.mappers.individuals;

import com.data.sagrilaft.mappers.entities.Entities;
import com.data.sagrilaft.mappers.entities.EntityPrev;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CONSOLIDATED_LIST")
@XmlAccessorType(XmlAccessType.FIELD)
public class Consolidated {

  @XmlElement(name = "INDIVIDUALS")
  private Individuals individuals = null;

  @XmlElement(name = "ENTITIES")
  private Entities entityPrev = null;

  public Individuals getIndividuals(){
    return individuals;
  }

  public Entities getEntityPrev(){
    return entityPrev;
  }

}
