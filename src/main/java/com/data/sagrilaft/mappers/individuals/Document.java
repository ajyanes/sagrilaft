package com.data.sagrilaft.mappers.individuals;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "INDIVIDUAL_DOCUMENT")
@XmlAccessorType(XmlAccessType.FIELD)
public class Document implements Serializable {

  @XmlElement(name = "TYPE_OF_DOCUMENT")
  private String documentType="";

  @XmlElement(name = "NUMBER")
  private String number="";

  @Override
  public String toString() {
    return "Document{" +
        "documentType='" + documentType + '\'' +
        ", number='" + number + '\'' +
        '}';
  }
}
