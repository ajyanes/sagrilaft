package com.data.sagrilaft.mappers.individuals;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "INDIVIDUAL_DATE_OF_BIRTH")
@XmlAccessorType(XmlAccessType.FIELD)
public class Birth implements Serializable {

  @XmlElement(name = "DATE")
  private String birthDate="";

  @XmlElement(name = "TYPE_OF_DATE")
  private String typeDate="";

  @Override
  public String toString() {
    return "Birth{" +
        "birthDate='" + birthDate + '\'' +
        ", typeDate='" + typeDate + '\'' +
        '}';
  }
}
