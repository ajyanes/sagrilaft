package com.data.sagrilaft.mappers.individuals;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "INDIVIDUALS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Individuals {

  @XmlElement(name = "INDIVIDUAL")
  private List<Individual> individuals = null;

  public List<Individual> getIndividuals(){
    return individuals;
  }

}
