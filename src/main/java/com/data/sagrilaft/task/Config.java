package com.data.sagrilaft.task;

import com.data.sagrilaft.service.EntityService;
import com.data.sagrilaft.service.IndividualService;
import java.io.IOException;
import java.net.HttpURLConnection;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Config {

  private Integer code;
  @Autowired
  IndividualService individualService;

  @Autowired
  EntityService entityService;

  @PostConstruct
  void startProcess(){
    log.info("Validando URL");
    try{
      code = individualService.validateUrl();
    } catch (IOException e) {
      e.printStackTrace();
    }

    if(code == HttpURLConnection.HTTP_OK){
      log.info("URL disponible");

      log.info("Inicio borrado de datos");
      individualService.deleteIndividuals();
      entityService.deleteEntity();
      log.info("Fin borrado de datos");
      log.info("Inicio insercion nuevos datos");
      individualService.saveIndividuals();
      entityService.saveEntity();
      log.info("Fin insercion nuevos datos");
    }else{
      log.info("URL NO DISPONIBLE. INTENTE MAS TARDE!!");
    }


  }

}
