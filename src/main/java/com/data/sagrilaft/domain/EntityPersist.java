package com.data.sagrilaft.domain;

import com.data.sagrilaft.mappers.entities.ListTypeEnt;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "onu_entities")
public class EntityPersist implements Serializable {

  @Id
  @Column(name = "ID")
  private String dataId;
  private Integer versionnum;
  private String firstname;
  private String unlisttype;
  private String referencenumber;
  private Date listedon;
  private String comments;
  private String listtype;
  private String lastdateupdate;
  private String alias;
  private String city;
  private String country;

}
