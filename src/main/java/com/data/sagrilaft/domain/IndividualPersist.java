package com.data.sagrilaft.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "onu_individuals")
public class IndividualPersist implements Serializable {

  @Id
  @Column(name = "ID")
  private String dataId;
  private Integer versionnum;
  private String firstname;
  private String secondname;
  private String thirdname;
  private String unlisttype;
  private String referencenumber;
  private Date listedon;
  private String comments;
  private String designation;
  private String nationality;
  private String listtype;
  private String lastdateupdate;
  private String alias;
  private String indaddress;
  private String datebirth;
  @Column(name = "typedate")
  private String typebirth;
  private String documenttype;
  private String documentnumber;

}
