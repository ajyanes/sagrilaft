package com.data.sagrilaft.repository.impl;

import com.data.sagrilaft.repository.IndividualRepository;
import com.data.sagrilaft.repository.IndividualRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource("classpath:sentences.xml")
public class IndividualRepositoryCustomImpl implements IndividualRepositoryCustom{

  @Autowired
  EntityManager em;

  @Autowired
  private Environment env;

  @Override
  public void deleteIndividualData() {
    String query = env.getProperty("deleteIndividual.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.executeUpdate();
  }

}
