package com.data.sagrilaft.repository.impl;

import com.data.sagrilaft.repository.EntityRepositoryCustom;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource("classpath:sentences.xml")
public class EntityRepositoryCustomImpl implements EntityRepositoryCustom {

  @Autowired
  EntityManager em;

  @Autowired
  private Environment env;

  @Override
  public void deleteEntityData() {
    String query = env.getProperty("deleteEntities.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.executeUpdate();
  }
}
