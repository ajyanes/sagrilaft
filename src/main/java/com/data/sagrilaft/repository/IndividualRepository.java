package com.data.sagrilaft.repository;

import com.data.sagrilaft.domain.IndividualPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndividualRepository extends CrudRepository<IndividualPersist, Long>, IndividualRepositoryCustom {

}
