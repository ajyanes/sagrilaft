package com.data.sagrilaft.repository;

import com.data.sagrilaft.domain.EntityPersist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityRepository  extends CrudRepository<EntityPersist, Long>, EntityRepositoryCustom {

}
