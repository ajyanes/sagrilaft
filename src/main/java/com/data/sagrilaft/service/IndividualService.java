package com.data.sagrilaft.service;


import java.io.IOException;

public interface IndividualService {

  void deleteIndividuals();

  void saveIndividuals();

  Integer validateUrl() throws IOException;

}
