package com.data.sagrilaft.service.impl;

import com.data.sagrilaft.domain.EntityPersist;
import com.data.sagrilaft.mappers.entities.Entities;
import com.data.sagrilaft.mappers.entities.EntityPrev;
import com.data.sagrilaft.mappers.individuals.Consolidated;
import com.data.sagrilaft.repository.EntityRepository;
import com.data.sagrilaft.service.EntityService;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EntityServiceImpl implements EntityService {

  @Value("${data.url}")
  private String url;

  @Autowired
  private EntityRepository entityRepository;

  @Override
  @Transactional
  public void deleteEntity() {
    entityRepository.deleteEntityData();
  }

  @Override
  public void saveEntity() {
    try {
      entityRepository.saveAll(getData());
    }catch (Exception ex){
      ex.printStackTrace();
    }
  }

  public List<EntityPersist> getData() {
    List<EntityPersist> persistList = new ArrayList<>();
    try {
      JAXBContext context = JAXBContext.newInstance(Consolidated.class);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      URL ur = new URL(url);
      Consolidated cons = (Consolidated) unmarshaller.unmarshal(ur);
      Entities ents = cons.getEntityPrev();
      getListEntities(persistList, ents);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    return persistList;
  }

  private void getListEntities(List<EntityPersist> persistList, Entities ent) {
    for (EntityPrev enti : ent.getEntitys()) {
      EntityPersist ep = EntityPersist.builder()
          .dataId(enti.getDataId())
          .versionnum(enti.getVersionnum())
          .firstname(enti.getFirstname())
          .unlisttype(enti.getUnlisttype())
          .referencenumber(enti.getReferencenumber())
          .listedon(enti.getListedon())
          .comments(enti.getComments())
          .listtype(enti.getListtype().toString())
          .lastdateupdate(enti.getLastdateupdate())
          .alias(enti.getAlias())
          .city(enti.getAddress().getCity())
          .country(enti.getAddress().getCountry())
          .build();

      persistList.add(ep);
    }
  }
}
