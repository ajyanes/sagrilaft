package com.data.sagrilaft.service.impl;

import com.data.sagrilaft.mappers.individuals.Consolidated;
import com.data.sagrilaft.mappers.individuals.Individual;
import com.data.sagrilaft.domain.IndividualPersist;
import com.data.sagrilaft.mappers.individuals.Individuals;
import com.data.sagrilaft.repository.IndividualRepository;
import com.data.sagrilaft.repository.impl.IndividualRepositoryCustomImpl;
import com.data.sagrilaft.service.IndividualService;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IndividualServiceImpl implements IndividualService {

  @Value("${data.url}")
  private String url;

  @Autowired
  private IndividualRepository individualRepository;

  public List<IndividualPersist> getData() {
    List<IndividualPersist> persistList = new ArrayList<>();
    try {
      JAXBContext context = JAXBContext.newInstance(Consolidated.class);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      URL ur = new URL(url);
      Consolidated cons = (Consolidated) unmarshaller.unmarshal(ur);
      Individuals inds = cons.getIndividuals();
      getListIndividuals(persistList, inds);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    return persistList;
  }

  @Override
  @Transactional
  public void deleteIndividuals() {
    individualRepository.deleteIndividualData();
  }

  @Override
  public void saveIndividuals(){
    try {
      individualRepository.saveAll(getData());
    }catch (Exception ex){
      ex.printStackTrace();
    }
  }

  @Override
  public Integer validateUrl() throws IOException {
    URL route = new URL(url);
    HttpURLConnection huc = (HttpURLConnection) route.openConnection();

    return huc.getResponseCode();
  }

  private void getListIndividuals(List<IndividualPersist> persistList, Individuals inds) {
    for (Individual ind : inds.getIndividuals()) {
      IndividualPersist ip = IndividualPersist.builder()
          .dataId(ind.getDataId())
          .versionnum(ind.getVersionNum())
          .firstname(ind.getFirstName())
          .secondname(ind.getSecondName())
          .thirdname(ind.getThirdName())
          .unlisttype(ind.getUnListType())
          .referencenumber(ind.getReferenceNumber())
          .listedon(ind.getListedOn())
          .comments(ind.getComments())
          .designation(ind.getDesignation()==null ? "" : ind.getDesignation().toString())
          .nationality(ind.getNationality()==null ? "" : ind.getNationality().toString())
          .listtype(ind.getListType().toString())
          .lastdateupdate(ind.getLastDateUpdate())
          .alias(ind.getAlias())
          .indaddress(ind.getIndAddress())
          .datebirth(ind.getDateBirth()==null ? "" : ind.getDateBirth().getBirthDate())
          .typebirth(ind.getDateBirth()==null ? "" : ind.getDateBirth().getTypeDate())
          .documenttype(ind.getDocumentType()==null ? "" : ind.getDocumentType().getDocumentType())
          .documentnumber(ind.getDocumentType()==null ? "" : ind.getDocumentType().getNumber())
          .build();

      persistList.add(ip);
    }
  }

}
