package com.data.sagrilaft.service;

public interface EntityService {

  void deleteEntity();
  void saveEntity();

}
